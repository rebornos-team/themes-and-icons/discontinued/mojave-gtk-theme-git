# mojave-gtk-theme-git

A Mac OSX like theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Pantheon, XFCE, Mate, etc.

https://github.com/vinceliuice/Mojave-gtk-theme

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/themes-and-icons/mojave-gtk-theme-git.git
```

